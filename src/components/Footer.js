import React from 'react';
import logo from './logo.svg';

function Footer() {
  return (
    <div>
      <footer>
        Develop with <img src={logo} className="App-logo" alt="logo"/>
        More info on <a href="https://adisazizan.xyz">adisazizan.xyz</a> &copy; 2019
      </footer>
    </div>
);
}

export default Footer;
