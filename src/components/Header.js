import React from 'react';

function Header() {
  return (
    <div>
      <header className="App-header">
        <nav className="mynav">
          <ul>
            <li><a href="/"> Home </a></li>
            <li><a href="/education"> Education </a></li>
            <li><a href="/experience"> Experience </a></li>
            <li><a href="/skills"> Skills </a></li>
            <li><a href="/videos"> My Video </a></li>
          </ul>
        </nav>
        </header>
    </div>
);
}
export default Header;
