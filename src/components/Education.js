import React from 'react';

function Education(){
  return (
    <div className="main">
      <h1>Education</h1>
      <p>2011 – 2015 BACHELOR OF COMPUTER SCIENCE</p>
      <p>INTERNATIONAL ISLAMIC UNIVERSITY MALAYSIA (IIUM)</p>
      <p>CGPA : 3.1 </p>

      <h4>ACTIVITIES</h4>
      <ul>
        <li>Vice President of ICT Student Society (2014)</li>
        <li>Third Place - Game Salad for Game Programming (2014)</li>
        <li>Network Programming Tutor (2014)</li>
        <li>Consolation Prize - Hackathon Open Data in TM Innovation Center (2015)</li>
      </ul>
    </div>
  );
}
export default Education;
