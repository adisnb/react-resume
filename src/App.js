import React from 'react';
import './App.css';
import Header from './components/Header';
import Footer from './components/Footer';
import Education from './components/Education';
import Experience from './components/Experience';
import Skills from './components/Skills';
import Video from './components/Video';
import { BrowserRouter as Router, Route } from "react-router-dom";

function App() {
  return (
    <div>
    <Header />
    <div className="content">
      <div className="profile">
        <h2>Profile</h2>
        <p>Experience in open source technology and fast learner to develop web applications or software.
        Enthusiastic in learning new technology and have many experiences as
        team leader and web application development.
        Familiar with both Agile Framework and Waterfall software development
        methodology including conducting Scrum meeting.</p>
        <div className="line" />
      </div>
    <Router>
        <Route exact path="/" />
        <Route path="/education" component={Education} />
        <Route path="/experience" component={Experience} />
        <Route path="/skills" component={Skills} />
        <Route path="/videos" component={Video} />
    </Router>
    </div>
    <Footer />
    </div>

  );
}

export default App;
